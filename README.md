# N-Queen Problem (Python)

## Introduction
This is an application which computes solutions to the N-queen problem.

## Requirements

This project depends on
* [Python](https://www.python.org/)

## Install

To install Python on Ubuntu execute the following commands in the terminal

1.

```
sudo apt update
```

2.

```
sudo apt install python3
```

3. Navigate to the folder where the Python-file is located and run the following command to execute the program:

```
python3 main.py
```

## Contributing
@joelfredin


