import n_python_function



def odd_list(size_of_chessboard):
    #temporary_list = range(1, size_of_chessboard+1)
    new_list = []
    for x in range(1, size_of_chessboard+1):
        if x%2 == 1:
            new_list.append(x)
    return new_list


def modulo6(size_of_chessboard):
    even_numbers = even_list(size_of_chessboard)
    odd_numbers = odd_list(size_of_chessboard)
    full_list = even_numbers + odd_numbers
    
    return full_list

def modulo_6_modeq2(size_of_chessboard):
    even_numbers = even_list(size_of_chessboard)
    odd_numbers = odd_list(size_of_chessboard)

    odd_numbers.pop(0)
    odd_numbers.pop(0)
    odd_numbers.pop(0)
    odd_numbers.insert(0,1)
    odd_numbers.insert(0,3)
    odd_numbers.append(5)
    full_list = even_numbers + odd_numbers

    return full_list


def modulo_6_modeq3(size_of_chessboard):
    even_numbers = even_list(size_of_chessboard)
    odd_numbers = odd_list(size_of_chessboard)

    even_numbers.pop(0)
    even_numbers.append(2)
    odd_numbers.pop(0)
    odd_numbers.pop(0)
    odd_numbers.append(1)
    odd_numbers.append(3)
    full_list = even_numbers + odd_numbers

    return full_list


def create_chessboard(size_of_chessboard):
    row = []
    chessboard = []

    if size_of_chessboard % 6 != 2 and size_of_chessboard % 6 != 3:
        for i in range(0,len(modulo6(size_of_chessboard))):
            for j in range(1,len(modulo6(size_of_chessboard))+1):
                if j == modulo6(size_of_chessboard)[i]:
                    row.append(modulo6(size_of_chessboard)[i]+1)
                else:
                    row.append(0)

            chessboard.append(row)
            row = []
    if size_of_chessboard % 6 == 2:
        for i in range(0,len(modulo_6_modeq2(size_of_chessboard))):
            for j in range(1,len(modulo_6_modeq2(size_of_chessboard))+1):
                if j == modulo_6_modeq2(size_of_chessboard)[i]:
                    row.append(modulo_6_modeq2(size_of_chessboard)[i]+1)
                else:
                    row.append(0)

            chessboard.append(row)
            row = []

    if size_of_chessboard % 6 == 3:
        for i in range(0,len(modulo_6_modeq3(size_of_chessboard))):
            for j in range(1,len(modulo_6_modeq3(size_of_chessboard))+1):
                if j == modulo_6_modeq3(size_of_chessboard)[i]:
                    row.append(modulo_6_modeq3(size_of_chessboard)[i]+1)
                else:
                    row.append(0)

            chessboard.append(row)
            row = []

    return chessboard

def visualize_board(my_chessboard):
    for list in my_chessboard:
        print(*list)


def chess_two_by_two():
    chessboard = [[1,0],[0,2]]
    chessboard2 = [[0,2],[1,0]]
    visualize_board(chessboard)
    print("")
    visualize_board(chessboard2)


def chess_three_by_three():
    chessboard = [[1,0,0],[0,2,0], [0,0,3]]
    for i in range(0,len(chessboard)):
        chessboard[i] = [1,0,0]
        for j in range(0,len(chessboard)):
            if j == i:
                continue
            chessboard[j] = [0,2,0]
            for k in range(0, len(chessboard)):
                if k == i or k == j:
                    continue
                chessboard[k] = [0,0,3]
                visualize_board(chessboard)
                print("")


#the_chessboard = create_chessboard(5)
#visualize_board(the_chessboard)

dimension_of_board = int(input("What size of the board do you want to consider?"))

if(dimension_of_board == 1):
    print("Here is the only valid chessboard of size 1:")
    visualize_board([[1]])

if(dimension_of_board == 2):
    print("Ahh, you entered the number 2, as you can see there are just two boards, and no one is a valid board.")
    chess_two_by_two()
if(dimension_of_board == 3):
    print("Ahh, you entered the number 3, as you can see there are six boards with a single queen placed at every row and column, each of which is not valid.")
    chess_three_by_three()
if(dimension_of_board > 3):
    print("Here is a valid board of size" + str(dimension_of_board) + ":")
    chessboard = create_chessboard(dimension_of_board)
    visualize_board(chessboard)